# Gosu Game Jam 5
- game jam: https://itch.io/jam/gosu-game-jam-5
- gem: (TODO link to RubyGems)
- repository: https://gitlab.com/rasunadon/ggj5
- bug tracker: https://gitlab.com/rasunadon/ggj5/issues
- licence: CC-BY-SA 3.0, Detros
- email: rasunadon@seznam.cz

_GGJ5_ (TODO) was created during the fifth Gosu Game Jam which run for
two weeks between 2023-10-07 and 2023-10-22. (TODO) Your goal...


# Controls
- movement: mouse
- quit: Esc
(TODO)


# Other documentation
- CHANGELOG: list of what has been added/changed/fixed/removed in given version

